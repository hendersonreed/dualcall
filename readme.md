## DualCall

This is a simple webpage designed to make it easier to collaboratively work with visual aids like whiteboards. Simply enter a room name in the text input and hit the button, and you'll be dropped into a video call and collaborative whiteboard session.

### Constructing a room url:

Rooms are identified only by their room name, which is purely alphanumeric characters(`"^[a-zA-Z0-9]*$"` is the regex.) So all you have to do is navigate to `website.com/path/to/session.html?<put the room name here>`.

Note that the more entropy (more bits/characters) your room name has, the less likely it is that someone else might stumble into it. Because these aren't password protected, you'll definitely want to be careful about this.

### Underlying Technologies:

This is a very simple page, constructed out of two iframes and a teeny tiny bit of Javascript gluing it all together.

The whiteboard is provided by [Excalidraw](https://excalidraw.com) and the video call is from [Jitsi](https://meet.jit.si), both of which are great open source tools.

### troubleshooting:

* I just get a blank frame where Jitsi is supposed to be.
    * This could be caused by any ad-blockers that you have enabled (working to resolve - this is NOT ideal).
* I can't share my screen inside Jitsi.
    * Unfortunately, the best answer here is to try out Chromium-based browsers. I'm a FireFox user myself, so I am trying to resolve this.
